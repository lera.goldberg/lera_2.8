
let images = [
    'ma.png','ch.jpg', 'r.jpg', 'x.jpg'
]
let text = [
    'Тибетский мастиф - экстремально большая собака с «львиной» гривой.Рост — 60—77 см.', 'Чау-чау — сторожевая собака, компаньон, одна из древнейших пород собак', 'Ротвейлер - это собака большого размера, с сильным и мускулистым телом','Хаски - это общее название для нескольких пород ездовых собак. '
]
let num = 0;

function forwImg() {
    let slider = document.getElementById('slider');
    let text1 = document.getElementById('text1');
    num++;
    
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
    text1.innerText = text[num];
};


function backImg() {
    let slider = document.getElementById('slider');
    let text1 = document.getElementById('text1');
    num--;
    
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];
    text1.innerText = text[num];

};

function handleClick() {
    const inputValue = document.getElementById("text2");
    if (inputValue.value === '') {
        alert("Вы должны что-то написать!");
      } else {

        const li = document.createElement("li");
        const div = document.createElement("div");

        li.appendChild(div);
    
        div.innerText = inputValue.value;
        const ul = document.getElementById("list");
        ul.appendChild(li);
        inputValue.value = ''
      }
};